from django.test import TestCase
from rest_framework.test import APIClient


class UserAuthAPITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_signup(self):
        response = self.client.post('/auth/signup', {'username': 'test', 'password': 'test'}, 'json')

        self.assertEqual(response.status_code, 201)
        self.assertTrue(isinstance(response.json()['access'], str))
        self.assertTrue(isinstance(response.json()['refresh'], str))

    def test_signin(self):
        user = {'username': 'test', 'password': 'test'}
        self.client.post('/auth/signup', user, 'json')

        response = self.client.post('/auth/signin', user, 'json')
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + response.json()['access'])

        response = self.client.get('/loans/')

        self.assertEqual(response.status_code, 200)

    def test_refresh(self):
        user = {'username': 'test', 'password': 'test'}
        response = self.client.post('/auth/signup', user, 'json')

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + response.json()['refresh'])
        response = self.client.post('/auth/refresh', user, 'json')

        self.assertEqual(response.status_code, 200)
        self.assertTrue(isinstance(response.json()['access'], str))
