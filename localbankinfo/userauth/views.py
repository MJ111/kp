from tokenize import TokenError

from django.contrib.auth import authenticate
from django.db import IntegrityError
from django.http import JsonResponse
from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.state import User
from rest_framework_simplejwt.tokens import RefreshToken


def get_tokens_for_user(user) -> dict:
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class SignUpView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        username = request.data['username']
        pw = request.data['password']

        try:
            user = User.objects.create_user(username=username, password=pw)
            user.save()
        except IntegrityError:
            return JsonResponse({'error': 'The username is unique.'}, status=status.HTTP_400_BAD_REQUEST)

        token = get_tokens_for_user(user)

        return JsonResponse(token, status=status.HTTP_201_CREATED)


class SignInView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        username = request.data['username']
        pw = request.data['password']

        user = authenticate(username=username, password=pw)
        if user is None:
            return JsonResponse({'errors': 'The user is not valid.'}, status=status.HTTP_401_UNAUTHORIZED)

        token = get_tokens_for_user(user)

        return JsonResponse(token, status=status.HTTP_200_OK)


class RefreshView(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def post(self, request):
        auth_header = request.META.get('HTTP_AUTHORIZATION', None)
        if not auth_header or 'Bearer ' not in auth_header:
            raise InvalidToken()

        token = auth_header.split('Bearer ')[1]

        serializer = TokenRefreshSerializer(data={'refresh': token})

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return JsonResponse(serializer.validated_data, status=status.HTTP_200_OK)
