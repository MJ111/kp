from django.db import models

from .utils import get_amount, get_avg_rate


class RegionManager(models.Manager):
    def get_matched_region_geo(self, text: str) -> list:
        regions = Region.objects.all()
        for reg in regions:
            if reg.region[:2] in text:
                return [reg.lon, reg.lat]
        return [0, 0]


class LoanManager(models.Manager):
    def get_filtered_loan(self, limit: int, rate: float, text: str) -> list:
        loans = Loan.objects.all().prefetch_related('region')

        filtered_loans = []
        for loan in loans:
            _limit = get_amount(loan.limit)
            _rate = get_avg_rate(loan.rate)
            if _limit >= limit and _rate <= rate:
                usages = loan.usage.replace('및', '').split(' ')

                for u in usages:
                    if u and u in text:
                        filtered_loans.append(loan)
                        break
        return filtered_loans


class Region(models.Model):
    """
    지원 지자체 엔티티
    """
    id = models.AutoField(primary_key=True)

    region = models.CharField(max_length=127, unique=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)

    objects = RegionManager()

    class Meta:
        ordering = ['created_time']

    def __str__(self):
        return self.region


class Loan(models.Model):
    """
    지자체 지원정보 엔티티
    """
    id = models.AutoField(primary_key=True)

    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    target = models.TextField()
    usage = models.CharField(max_length=127)
    limit = models.CharField(max_length=127)
    rate = models.CharField(max_length=127)
    institute = models.CharField(max_length=127)
    mgmt = models.CharField(max_length=255)
    reception = models.CharField(max_length=255)

    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)

    objects = LoanManager()

    class Meta:
        ordering = ['created_time']
