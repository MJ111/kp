import re

from django.http import JsonResponse
from rest_framework import generics
from rest_framework.views import APIView

from .serializers import LoanSerializer, LoanRecommendationSerializer
from .models import Loan, Region
from .utils import get_unique_in_order, sort_loans_with_limit_rate, sort_loans_with_rate, parse_loans_csv, get_amount, \
    get_sorted_loans_with_geo


class LoanCreateView(APIView):
    def post(self, request):
        """
        create new loans with json input
        """
        serializer = LoanSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201, safe=False)
        return JsonResponse(serializer.errors, status=400, safe=False)


class LoanUploadView(APIView):
    def post(self, request):
        """
        create new loans with csv file input
        """
        loans = parse_loans_csv(request.FILES['data'])

        serializer = LoanSerializer(data=loans, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201, safe=False)
        return JsonResponse(serializer.errors, status=400, safe=False)


class LoanRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    serializer_class = LoanSerializer

    def get_queryset(self):
        _id = self.kwargs['pk']
        return Loan.objects.filter(id=_id)


class LoanSortView(APIView):
    def get(self, request):
        """
        get unique sorted region.
        sort condition: limit DESC and rate ASC
        """
        number = int(request.GET.get('n', 10))

        loans = Loan.objects.all() \
            .prefetch_related('region')

        loans = [{'limit': loan.limit, 'rate': loan.rate, 'region': loan.region.region}
                 for loan in loans]

        loans = sort_loans_with_limit_rate(loans)

        regions = list(map(lambda x: x['region'], loans))
        regions = get_unique_in_order(regions)

        return JsonResponse({'result': regions[:number]})


class LoanListView(generics.ListAPIView):
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer


class LoanSearchView(generics.ListAPIView):
    serializer_class = LoanSerializer

    def get_queryset(self):
        region = self.request.data['region']
        return Loan.objects.filter(region__region=region)

    def post(self, request, *arg, **kwargs):
        """
        get loans matches region name input
        """
        return self.list(request, *arg, **kwargs)


class LoanTopView(APIView):
    def get(self):
        """
        get region which has smallest rate
        """
        loans = Loan.objects.all() \
            .prefetch_related('region')

        loans = [{'rate': loan.rate, 'region': loan.region.region}
                 for loan in loans]

        result = sort_loans_with_rate(loans)[0]

        return JsonResponse({'result': result['region']})


class LoanRecommendationView(APIView):
    def post(self, request):
        """
        get recommended loan with text which is describing user's needs
        """
        text = request.data['text']

        lon, lat = Region.objects.get_matched_region_geo(text)

        _limit = re.findall(r'\d+ ?[억|백만]', text)
        limit = get_amount(_limit[0].replace(' ', '')) if len(_limit) else 500000

        _rate = re.findall(r'\d*\.?\d+', text)
        rate = float(_rate[0].split('%')[0]) if len(_rate) else 3

        filtered_loans = Loan.objects.get_filtered_loan(limit, rate, text)

        sorted_loans = get_sorted_loans_with_geo(filtered_loans, lon, lat)

        serializer = LoanRecommendationSerializer(sorted_loans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
