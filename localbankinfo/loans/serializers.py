import random

from rest_framework import serializers

from .models import Loan, Region


class LoanSerializer(serializers.ModelSerializer):
    region = serializers.CharField()

    class Meta:
        model = Loan
        fields = ['target', 'usage', 'limit', 'rate', 'institute', 'mgmt', 'reception', 'region']

    def create(self, validated_data):
        region_name = validated_data.pop('region')
        region = Region.objects.get_or_create(region=region_name,
                                              defaults={'lon': random.randint(-100, 100),
                                                        'lat': random.randint(-100, 100)})[0]
        return Loan.objects.create(region=region, **validated_data)

    def update(self, instance, validated_data):
        region_data = validated_data.pop('region', None)

        if region_data:
            raise serializers.ValidationError('region could not be updated.')

        return super().update(instance, validated_data)


class LoanRecommendationSerializer(serializers.ModelSerializer):
    region = serializers.CharField()

    class Meta:
        model = Loan
        fields = ['usage', 'limit', 'rate', 'region']
