from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework_simplejwt.state import User

from .models import Region, Loan
from .utils import get_avg_rate, get_amount, get_unique_in_order, sort_loans_with_limit_rate, sort_loans_with_rate, \
    parse_loans_csv, get_sorted_loans_with_geo


class UtilTestCase(TestCase):
    def test_get_avg_rate(self):
        rate = '대출이자 전액'
        self.assertEqual(get_avg_rate(rate), 0)

        rate = '2.8%'
        self.assertEqual(get_avg_rate(rate), 2.8)

        rate = '1.5%~2.8%'
        self.assertEqual(get_avg_rate(rate), 2.15)

        rate = '0.0%~3.0%'
        self.assertEqual(get_avg_rate(rate), 1.5)

    def test_get_amount(self):
        limit = '추천금액 이내'
        self.assertEqual(get_amount(limit), 0)

        limit = '50백만원 이내'
        self.assertEqual(get_amount(limit), 5000)

        limit = '3억원 이내'
        self.assertEqual(get_amount(limit), 30000)

        limit = '10억원 이내'
        self.assertEqual(get_amount(limit), 100000)

    def test_get_unique(self):
        l = ['a', 'b', 'c', 'a', 'c', 'b', 'd']
        self.assertEqual(get_unique_in_order(l), ['a', 'b', 'c', 'd'])

        l = ['d', 'c', 'a', 'b', 'c']
        self.assertEqual(get_unique_in_order(l), ['d', 'c', 'a', 'b'])

        self.assertEqual(get_unique_in_order([]), [])

    def test_sort_loans_with_limit_rate(self):
        loans = [{'limit': '5', 'rate': '1.5%'},
                 {'limit': '2', 'rate': '0.4%'},
                 {'limit': '10', 'rate': '1%'}]

        self.assertEqual(sort_loans_with_limit_rate(loans),
                         [{'limit': '10', 'rate': '1%'},
                          {'limit': '5', 'rate': '1.5%'},
                          {'limit': '2', 'rate': '0.4%'}])

        loans = [{'limit': '5', 'rate': '1.5%'},
                 {'limit': '5', 'rate': '0.0%~3.0%'},
                 {'limit': '5', 'rate': '0.2%'}]

        self.assertEqual(sort_loans_with_limit_rate(loans),
                         [{'limit': '5', 'rate': '0.2%'},
                          {'limit': '5', 'rate': '1.5%'},
                          {'limit': '5', 'rate': '0.0%~3.0%'}])

    def test_sort_loans_with_rate(self):
        loans = [{'rate': '1.5%'},
                 {'rate': '0.0%~3.0%'},
                 {'rate': '0.2%'},
                 {'rate': '0.1%'}]

        self.assertEqual(sort_loans_with_rate(loans),
                         [{'rate': '0.1%'},
                          {'rate': '0.2%'},
                          {'rate': '1.5%'},
                          {'rate': '0.0%~3.0%'}])

    def test_parse_loans_csv(self):
        csv = ['desc line\n'.encode(),
               '11,광주광역시,"광주광역시 소재 중소기업(소상공인, 벤처기업 포함)으로서 광주광역시장이 추천한자",운전,5억원 이내,2.0%~3.0%,"광주광역시 경제고용진흥원, 광주신용보증재단, 광주광역시",호남지역본부,전 영업점\n'.encode()]

        self.assertEqual(parse_loans_csv(csv),
                         [{'region': '광주광역시',
                           'target': '"광주광역시 소재 중소기업(소상공인, 벤처기업 포함)으로서 광주광역시장이 추천한자"',
                           'usage': '운전', 'limit': '5억원 이내',
                           'rate': '2.0%~3.0%', 'institute': '"광주광역시 경제고용진흥원, 광주신용보증재단, 광주광역시"',
                           'mgmt': '호남지역본부',
                           'reception': '전 영업점'}])

    def test_get_sorted_loans_with_geo(self):
        def get_obj(lon, lat, id):
            class Object(object):
                pass

            a = Object()
            a.usage = 'usage'
            a.rate = 'rate'
            a.limit = 'limit'
            a.region = Object()
            a.region.id = id
            a.region.lon = lon
            a.region.lat = lat

            return a

        loans = [get_obj(-5.09, 2.11, '1'), get_obj(32, -43, '2'), get_obj(500, -3.54, '3'),
                 get_obj(25.33, -31.42, '4')]

        result = get_sorted_loans_with_geo(loans, 0, 0)

        def get_result(loans):
            return [{'region': l.region.id, 'usage': l.usage, 'limit': l.limit, 'rate': l.rate} for l in loans]

        self.assertEqual(result, get_result([loans[0], loans[3], loans[1], loans[2]]))


class LoanAPITestCase(TestCase):
    def setUp(self):
        from userauth.views import get_tokens_for_user
        user = User.objects.create_user(username='test', password='test')
        token = get_tokens_for_user(user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token['access'])

        self.loans = [{'region': '광주광역시',
                       'target': '"광주광역시 소재 중소기업(소상공인, 벤처기업 포함)으로서 광주광역시장이 추천한자"',
                       'usage': '운전', 'limit': '5억원 이내',
                       'rate': '2.0%~3.0%', 'institute': '"광주광역시 경제고용진흥원, 광주신용보증재단, 광주광역시"',
                       'mgmt': '호남지역본부',
                       'reception': '전 영업점'}]

    def test_create(self):
        response = self.client.post('/loans/create', self.loans, 'json')

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), self.loans)

    def test_create_region_validation_error(self):
        # region cannot be omitted
        loans = [{**self.loans[0]}]
        del loans[0]['region']
        response = self.client.post('/loans/create', loans, 'json')

        self.assertEqual(response.status_code, 400)

    def test_update(self):
        self.client.post('/loans/create', self.loans, 'json')

        patch_data = {'reception': '호남 전 영업점'}
        response = self.client.patch('/loans/1', patch_data, 'json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {**self.loans[0], **patch_data})

    def test_update_region_validation_error(self):
        # region cannot be updated for data integrity

        self.client.post('/loans/create', self.loans, 'json')

        patch_data = {'region': '서울시'}
        response = self.client.patch('/loans/1', patch_data, 'json')

        self.assertEqual(response.status_code, 400)

    def test_sort(self):
        loans = [
            {
                "target": "\"기보에서 발행한 ‘기술신용정보(TCB)’의 기술신용등급이 신용등급(기보 기준)보다 높은 기업, 기보에서 발행한 ‘기술신용정보(TCB)’의 기술등급이 T4등급 이상인 기업\"",
                "usage": "운전",
                "limit": "5억원 이내",
                "rate": "1%~3%",
                "institute": "기술신용보증기금",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "기술신용보증기금"
            },
            {
                "target": "기술신용보증기금으로부터 “고용환경개선” 용도로 자금추천을 받은 자",
                "usage": "시설",
                "limit": "추천금액 이내",
                "rate": "4.00%",
                "institute": "기술신용보증기금",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "기술신용보증기금"
            },
            {
                "target": "업무용 부동산(비거주용)에 대하여 에너지 성능개선을 위한 리모델링을 구상 또는 실행중인 기업으로 국토교통부의 융자추천을 받은 기업",
                "usage": "시설",
                "limit": "50억원 이내",
                "rate": "2%~4%",
                "institute": "한국토지주택공사",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "국토교통부"
            },
            {
                "target": "안양상공회의소에서 추천하는 자",
                "usage": "운전",
                "limit": "5억원 이내",
                "rate": "1.00%",
                "institute": "안양상공회의소",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "안양상공회의소"
            }
        ]

        self.client.post('/loans/create', loans, 'json')

        response = self.client.get(f'/loans/sort')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'result': ['국토교통부', '안양상공회의소', '기술신용보증기금']})


class RegionManagerTestCase(TestCase):
    def test_get_matched_region_geo(self):
        Region(region='부천시', lon=3.6, lat=44.1).save()
        Region(region='김천시', lon=71, lat=9).save()

        lon, lat = Region.objects.get_matched_region_geo('부천시에 사는 김모씨')

        self.assertEqual([float(lon), float(lat)], [3.6, 44.1])


class LoanManagerTestCase(TestCase):
    def setUp(self):
        from userauth.views import get_tokens_for_user
        user = User.objects.create_user(username='test', password='test')
        token = get_tokens_for_user(user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token['access'])

    def test_get_filtered_loan(self):
        loans = [
            {
                "target": "\"기보에서 발행한 ‘기술신용정보(TCB)’의 기술신용등급이 신용등급(기보 기준)보다 높은 기업, 기보에서 발행한 ‘기술신용정보(TCB)’의 기술등급이 T4등급 이상인 기업\"",
                "usage": "운전",
                "limit": "5억원 이내",
                "rate": "1%~3%",
                "institute": "기술신용보증기금",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "기술신용보증기금"
            },
            {
                "target": "업무용 부동산(비거주용)에 대하여 에너지 성능개선을 위한 리모델링을 구상 또는 실행중인 기업으로 국토교통부의 융자추천을 받은 기업",
                "usage": "시설",
                "limit": "50억원 이내",
                "rate": "2%~4%",
                "institute": "한국토지주택공사",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "국토교통부"
            },
            {
                "target": "안양상공회의소에서 추천하는 자",
                "usage": "운전",
                "limit": "80억원 이내",
                "rate": "1.00%",
                "institute": "안양상공회의소",
                "mgmt": "여신기획부",
                "reception": "전영업점",
                "region": "안양상공회의소"
            }
        ]

        self.client.post('/loans/create', loans, 'json')

        result = Loan.objects.get_filtered_loan(50, 1.3, '운전')

        self.assertEqual(result[0].region.region, '안양상공회의소')
