from django.urls import path

from . import views

urlpatterns = [
    path('', views.LoanListView.as_view()),
    path('<int:pk>', views.LoanRetrieveUpdateView.as_view()),
    path('sort', views.LoanSortView.as_view()),
    path('top', views.LoanTopView.as_view()),
    path('search', views.LoanSearchView.as_view()),
    path('recommend', views.LoanRecommendationView.as_view()),
    path('upload', views.LoanUploadView.as_view()),
    path('create', views.LoanCreateView.as_view()),
]
