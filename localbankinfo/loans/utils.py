import math
import re
from functools import cmp_to_key


def get_avg_rate(rate: str) -> float:
    numbers = list(map(float, re.findall(r'\d*\.?\d+', rate)))
    if len(numbers):
        return sum(numbers) / len(numbers)
    else:
        return 0


def get_amount(limit: str) -> int:
    limit = limit.replace('억', '0000')
    limit = limit.replace('백만', '00')
    numbers = re.findall(r'\d+', limit)
    if len(numbers):
        return int(numbers[0])
    else:
        return 0


def get_unique_in_order(seq: list) -> list:
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def sort_loans_with_limit_rate(loans: list) -> list:
    def cmp(x, y):
        diff = get_amount(y['limit']) - get_amount(x['limit'])  # reverse order when limit
        if diff == 0:
            return get_avg_rate(x['rate']) - get_avg_rate(y['rate'])
        else:
            return diff

    return sorted(loans, key=cmp_to_key(cmp))


def sort_loans_with_rate(loans: list) -> list:
    def cmp(x, y):
        return get_avg_rate(x['rate']) - get_avg_rate(y['rate'])

    return sorted(loans, key=cmp_to_key(cmp))


def parse_loans_csv(uploaded_file) -> list:
    lines = [line.decode().rstrip('\n') for line in uploaded_file]
    lines = lines[1:]  # skip desc line

    loans = []
    for line in lines:
        split_str = re.split(',(?=(?:[^"]|"[^"]*")*$)', line)  # exclude comma in double quote
        loan = dict({'region': split_str[1], 'target': split_str[2], 'usage': split_str[3], 'limit': split_str[4],
                     'rate': split_str[5], 'institute': split_str[6], 'mgmt': split_str[7],
                     'reception': split_str[8].rstrip()})
        loans.append(loan)
    return loans


def get_sorted_loans_with_geo(loans: list, target_lon: float, target_lat: float) -> list:
    loans_with_region_diff = []
    for loan in loans:
        if loan.region.lon and loan.region.lat:
            diff = math.sqrt(math.pow(loan.region.lon - target_lon, 2) + math.pow(loan.region.lat - target_lat, 2))
            loans_with_region_diff.append({'loan': loan, 'diff': diff})
        else:
            loans_with_region_diff.append({'loan': loan, 'diff': 999})

    sorted_loans = sorted(loans_with_region_diff, key=lambda x: x['diff'])

    def map_loan(x):
        return {'region': x['loan'].region.id, 'usage': x['loan'].usage, 'limit': x['loan'].limit,
                'rate': x['loan'].rate}

    return list(map(map_loan, sorted_loans))
