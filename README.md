# 지자체 협약 지원 API 개발

## 개발 프레임워크

[django framework](https://www.djangoproject.com/), [django rest framework](https://www.django-rest-framework.org/) 사용

## 문제해결 방법

크게 Model, View, Serializer 로 구조가 나뉘어짐. Model 은 데이터를 담고,
Serializer 는 Model 을 이용하여 데이터 조작을 하며 데이터를 직렬화고,
View 는 Serializer 를 이용하여 입력에 대한 출력을 응답함.


`localbankinfo/loans/models.py` 에 Loan, Region 모델을 선언하여 각각 지자체 지원정보, 지원 지자체 데이터를 저장함.
Loan 모델의 region 필드는 Region 모델의 외래키로 설정하여 Region 데이터 참조함.


`localbankinfo/loans/serializers.py` 에 Loan Model 에 대한 LoanSerializer 를 선언하여 사용자 입력에 대한 데이터 유효성 테스트 후,
데이터를 생성하거나 수정하는 작업을 수행함. Loan Model 의 region 필드는 생성 이후에 수정되면 데이터 정합성이 틀어질 것 같아 region 수정 시도시 에러를 뱉음.


`localbankinfo/loans/views.py` 에 create, update, sort, search, list, top 등의 API View 를 선언함.
요청을 받아 Serializer 등을 이용하여 데이터를 가공하여 응답을 보냄.


지자체 지원정보를 추천하는 API 는 다음과 같은 로직으로 계산함. 
1. 입력 기사에 Region 모델의 데이터와 일치하는 지역 키워드가 있는지 확인하여 있다면 그 지역의 위도, 경도를 구함 
2. 입력 기사에 지원한도, 이차보전 값을 정규식으로 검사해서 구함 
3. 2번에서 구한 지원한도, 이차보전 값으로 지자체 지원정보 데이터를 필터링한 후 
해당 금융지원 용도 키워드를 입력 기사에서 포함하고 있다면 알맞은 지원정보로 판단함 
4. 3번에서 필터링한 지원정보 데이터를 1번에서 구한 해당 지역의 위도, 경도를 이용해 지역 기반으로 정렬함. 끝. 


JWT 기반 API 인증 기능은 [djangorestframework-simplejwt](https://github.com/davesque/django-rest-framework-simplejwt) 를 이용하여 개발함. 
`localbankinfo/userauth` 앱에 signup, signin, refresh 를 구현하였고 
`localbankinfo/localbankinfo/setting.py` 에 다음과 같이 JWT 인증을 디폴트로 설정함. 

```python 
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ]
```

django 의 User 모델을 이용하여 기본적으로 암호는 인코딩되어 저장됨. 또한 기본적으로 토큰은 `localbankinfo/localbankinfo/setting.py` 에 있는 SECRET_KEY 로 서명되어 생성됨.


## 빌드 및 실행 방법

### 0. 소스코드 다운로드

```bash
git clone git@gitlab.com:MJ111/kp.git
```

### 1. pyenv 과 python 설치

(이미 설치되어 있다면 스킵)

```bash
$ brew install pyenv
$ pyenv install -v 3.7.1
```

이런 에러가 떴다면, 
```
zipimport.ZipImportError: can't decompress data; zlib not available
```
이렇게 환경변수를 붙여서 설치함:
```bash
$ CFLAGS="-I$(brew --prefix openssl)/include -I$(xcrun --show-sdk-path)/usr/include" \
LDFLAGS="-L$(brew --prefix openssl)/lib" \
pyenv install -v 3.7.1
```

아래 코드를 쉘의 rc 파일에 추가함 (예를 들어, ~/.bashrc, ~/.zshrc)
```
eval "$(pyenv init -)"
export PYENV_VERSION=3.7.1
```

파이썬 버젼을 체크함
```bash
$ python --version # Python 3.7.1
```

### 2. 파이썬 패키지 설치

최상위 폴더에서 다음과 같은 커맨드를 실행

```bash
$ pip install -r requirements.txt
```

### 3. 데이터베이스(default 는 sqlite3)에 테이블 생성

```bash
$ python localbankinfo/manage.py migrate
```

### 4. 서버 실행

```bash
$ python localbankinfo/manage.py runserver localhost:8000
```

## 테스트 실행 방법

```bash
$ python localbankinfo/manage.py test
```

## API 상세
 
- 데이터 파일에서 각 레코드를 데이터베이스에 저장하는 API 개발

csv 파일로 업로드
```text
POST `/loans/upload` 
data: 사전과제1.csv 파일  
```

json 데이터로 생성
```text
POST `/loans`
data: 지자체 지원정보 json list
    ex) [{
            "target": "강릉시 소재 중소기업으로서 강릉시장이 추천한 자",
            "usage": "운전",
            "limit": "추천금액 이내",
            "rate": "3%",
            "institute": "강릉시",
            "mgmt": "강릉지점",
            "reception": "강릉시 소재 영업점",
            "region": "강릉시"
        },
        {
            "target": "강원도 소재 중소기업으로서 강원도지사가 추천한 자",
            "usage": "운전",
            "limit": "8억원 이내",
            "rate": "3%~5%",
            "institute": "강원도",
            "mgmt": "춘천지점",
            "reception": "강원도 소재 영업점",
            "region": "강원도"
        }]
``` 

- 지원하는 지자체 목록 검색 API 개발

```text
GET `/loans`
``` 

- 지원하는 지자체명을 입력 받아 해당 지자체의 지원정보를 출력하는 API 개발

```text
POST `/loans/search`
data: 지자체 정보 json 
    ex) {"region": "강릉시"}
```

- 지원하는 지자체 정보 수정 기능 API 개발

```text
PATCH `/loans/<pk>` 
data: 지자체 지원 정보 json
    ex) PATCH `/loans/1` data={"limit": "50억이내"}
```

- 지원한도 컬럼에서 지원금액으로 내림차순 정렬(지원금액이 동일하면 이차보전 평균 비율이 적은 순서)하여 특정 개수만 출력하는 API 개발

```text
GET `/loans/sort?n=N`
    ex) GET `/loans/sort?n=1`
```

- 이차보전 컬럼에서 보전 비율이 가장 작은 추천 기관명을 출력하는 API 개발

```text
GET `/loans/top`
```

- 특정 기사를 분석하여 본 사용자는 어떤 지자체에서 금융지원을 받는게 가장 좋을지 지 자체명을 추천하는 API 개발

```text
POST `/loans/recommend`
data: 기사 텍스트 json
    ex) {"text": "철수는 부천에 살고 있는데, 은퇴하고 시설 관리 비즈니스를 하기를 원한다. 시설 관리 관련 사업자들을 만나보니 관련 사업을 하려면 대체로 5 억은 필요하고, 이차보전 비율은 2% 이내가 좋다는 의견을 듣고 정부에서 운영하는 지자체 협약 지원정보를 검색한다."}
```
